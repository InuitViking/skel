<?php

namespace Inuitviking\Skel;

use Angus\Imms\php\classes\BaseComponent;
use Angus\Imms\php\classes\Bootstrapper;

class Skel extends BaseComponent {

	public array $subscribedEvents = [
		'page_load',
		'page_end'
	];

	public function __construct() {
		parse_ini_file(Bootstrapper::rootDirectory().'/src/plugins/inuitviking/skel/plugin.ini', true);
	}

	public function getNotification (string $event): void {
		if ($event === $this->subscribedEvents[0]) {
			$this->printPageLoad();
		} elseif ($event === $this->subscribedEvents[1]) {
			$this->printPageEnd();
		}
	}

	public function printPageLoad (): void {
		echo "THIS STRING IS LOADED AT PAGE_LOAD";
	}

	public function printPageEnd (): void {
		echo "THIS STRING IS LOADED AT PAGE_END";
	}
}