# Skel

This is an example plugin for inuitviking/imms.

## How to use

First make sure you're in your IMMS project, and then run:

```bash
composer require inuitviking/skel
```